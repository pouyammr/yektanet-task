import moment from 'moment';
import BinarySearchTree from "./BST";
import BSTNode from './Node';
import data from '../data.json';

const DB = new BinarySearchTree();
const insert = DB.insert.bind(DB);
export const parsedData = data.map((item: { [x: string]: string | number; }) => {
  return {
    id: item['id'],
    name: item['name'],
    date: moment(item['date']).valueOf(),
    title: item['title'],
    field: item['field'],
    oldValue: item['old_value'],
    newValue: item['new_value']
  }
})

for (const item of parsedData) {
  insert(new BSTNode(item));
}

export default DB;