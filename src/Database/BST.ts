import BSTNode , { NodeType } from "./Node";

class BinarySearchTree {
  root: NodeType

  constructor() {
    this.root = null;
  }

  inorder(node: NodeType) {
    const nodeArray = [];
    if (node !== null) {
      this.inorder(node.left);
      nodeArray.push(node.key);
      this.inorder(node.right);
    }
    return nodeArray;
  }

  min(node: NodeType): BSTNode {
    while (node?.left !== null)
      node = node?.left ?? null;
    return node;
  }

  max(node: NodeType): BSTNode {
    while (node?.right !== null)
      node = node?.right ?? null;
    return node;
  }

  successor(node: BSTNode): NodeType {
    if (node.right !== null)
      return this.min(node.right);
    let parentNode: NodeType = node.parent;
    while (parentNode !== null && node === parentNode.right) {
      node = parentNode;
      parentNode = parentNode.parent;
    }
    return parentNode;
  }

  predecessor(node: BSTNode): NodeType {
    if (node.left !== null)
    return this.max(node.left);
    let parentNode: NodeType = node.parent;
    while (parentNode !== null && node === parentNode.left) {
      node = parentNode;
      parentNode = parentNode.parent;
    }
    return parentNode;
  }

  insert(node: BSTNode): void {
    let y: NodeType = null;
    let x: NodeType = this.root;
    while (x !== null) {
      y = x;
      if (node.key.date < x.key.date)
        x = x.left
      else
        x = x.right
    }
    node.parent = y;
    if (y === null)
      this.root = node;
    else if (node.key.date < y.key.date)
      y.left = node;
    else
      y.right = node;
  }

  dateSort(type: "ascending" | "descending" | null) {
    const sortedTree = [];
    let successor : NodeType = this.min(this.root);
    let predecessor: NodeType = this.max(this.root);
    if (type !== null) {
      if (type === "ascending") {
        while (successor !== null) {
          sortedTree.push(successor.key);
          successor = this.successor(successor);
        }
      }
      else {
        while (predecessor !== null) {
          sortedTree.push(predecessor.key);
          predecessor = this.predecessor(predecessor);
        }
      }
    }
    return sortedTree;
  }

  dateSearch(date: number) {
    const search = (node: NodeType, date: number) => {
      while (node !== null && node.key.date !== date) {
        if (date < node.key.date)
        node = node.left;
        else
        node = node.right;
      }
      return node;
    }
    
    const searchArray = [];
    let searchNode: NodeType = search(this.root, date);
    
    while (searchNode !== null) {
      searchArray.push(searchNode.key);
      searchNode = search(searchNode.right, date);  
    }
    return searchArray;
  }

  querySort(queryType: "name" | "title" | "field" | "oldValue" | "newValue" ,type: "ascending" | "descending" | null) {
    const sortedArray = this.inorder(this.root);
    console.log(sortedArray);
    sortedArray.sort((a, b) => {
        if (type !== null) {
          if (type === "ascending") {
            if (a[queryType] < b[queryType])
              return -1;
            if (a[queryType] > b[queryType])
              return 1;

            return 0;
          }
          else {
            if (a[queryType] < b[queryType])
              return 1;
            if (a[queryType] > b[queryType])
              return -1;
          
            return 0;
          }
        }
        else
          return 0
      })
    return sortedArray;
  }

  querySearch(queryType: "name" | "field" | "title" , queryValue: string) {
    const nodeArray = this.inorder(this.root);
    const searchArray = nodeArray.filter(node => node[queryType] === queryValue);
    return searchArray;
  }
}

export default BinarySearchTree;