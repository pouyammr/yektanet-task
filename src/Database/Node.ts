export type NodeType = BSTNode | null;

class BSTNode {
  key: {
    id: number,
    name: string,
    date: number,
    title: string,
    field: "عنوان" | "قیمت",
    oldValue: string | number,
    newValue: string | number
  };
  parent: NodeType;
  left: NodeType;
  right: NodeType;
  
  constructor(key: any, parentNode?: BSTNode) {
    this.key = key;
    this.parent = parentNode ?? null;
    this.left = null;
    this.right = null;
  }
}

export default BSTNode;