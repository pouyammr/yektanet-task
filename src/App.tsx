import React, { useEffect, useState } from 'react';
import useSyncState from './hooks/useSyncState';
import moment from 'moment';
import DB, { parsedData } from 'src/Database/DB';
import { useHistory, useLocation } from 'react-router';
import './App.css';
import InputBox from './Components/InputBox';

const App: React.FC = () => {
  const history = useHistory();
  const { search } = useLocation();
  const [name, setName] = useState<string>("")
  const [date, setDate] = useState<string>("")
  const [title, setTitle] = useState<string>("")
  const [field, setField] = useState<string>("")
  const [checked, setChecked] = useState<string[]>(localStorage.getItem('checked')?.split(',') ?? []);
  const [itemArray, setItemArray] = useState(parsedData);
  const [sortType, setSortType] = useSyncState(Array(6).fill(null));

  const tableHeaders: ("name" | "title" | "field" | "oldValue" | "newValue")[] = ['name', 'title', 'field', 'oldValue', 'newValue'];

  const dateSearch = DB.dateSearch.bind(DB);
  const dateSort = DB.dateSort.bind(DB);
  const querySearch = DB.querySearch.bind(DB);
  const querySort = DB.querySort.bind(DB);

  useEffect(() => {
    if (search.length !== 0) {
      const parsedParams = Object.fromEntries(new URLSearchParams(search).entries());
      for (const query in parsedParams) {
        if (query !== date && (query === "name" || query === "title" || query === "field"))
          setItemArray(querySearch(query, parsedParams[query]))
        else
          setItemArray(dateSearch(moment(parsedParams[query]).unix()))
      }
    }
    else {
      setItemArray(parsedData);
    }
  }, [search])

  const handleSubmit = () => {
    const nameQuery = name.length > 0 ? {name: name} : null;
    const dateQuery = date.length > 0 ? {date: date} : null;
    const titleQuery = title.length > 0 ? {title: encodeURIComponent(title)} : null;
    const fieldQuery = field.length > 0 ? {field: encodeURIComponent(field)} : null;
    const query = {...nameQuery, ...dateQuery, ...titleQuery, ...fieldQuery}
    const searchParams = new URLSearchParams(query);
    history.replace(`?${searchParams}`)
  }

  const handleCancel = () => {
    history.replace('/');
  }

  const handleSort = (index: number) => {
    setSortType((prev: (null | 'ascending' | 'descending')[]) => prev[index] === 'descending' || prev[index] === null ? [...Array(index).fill(null), 'ascending', ...Array(5 - index).fill(null)] : [...Array(index).fill(null), 'descending', ...Array(5 - index).fill(null)])
    if (index === 1)
      setItemArray(dateSort(sortType[index]))
    else if (index === 0)
      setItemArray(querySort(tableHeaders[0], sortType[index]))
    else
      setItemArray(querySort(tableHeaders[index - 1], sortType[index]))
  }

  const handleChecked = (event: React.ChangeEvent<HTMLInputElement>, id: string) => {
    if (event.currentTarget.checked) {
      setChecked(prev => [...prev, id]);
      localStorage.setItem('checked', checked.join(','));
    }
    else {
      setChecked(prev => prev.splice(prev.indexOf(id), 1));
      localStorage.setItem('checked', checked.join(','));
    }
  }
  return (
    <div className="App">
      <div className="Container">
        <div className="search-fields">
          <div className="inputs">
            <InputBox title="نام تغییر دهنده" input={name} onChange={(value: string) => setName(value)} placeholder="نام تغییر دهنده" />
            <InputBox title="تاریخ" input={date} onChange={(value: string) => setDate(value)} placeholder="تاریخ" />
            <InputBox title="نام آگهی" input={title} onChange={(value: string) => setTitle(value)} placeholder="نام آگهی" />
            <InputBox title="فیلد" input={field} onChange={(value: string) => setField(value)} placeholder="فیلد" />
          </div>
          <div className="button-row">
            <button className="submit" onClick={() => handleSubmit()}>تایید</button>
            <button className="cancel" onClick={() => handleCancel()}>پاک کردن</button>
          </div>
        </div>
        <table className="table">
          <thead className="table-head">
            <tr>
              <td />
              <td onClick={() => handleSort(0)}>نام تغییردهنده</td>
              <td onClick={() => handleSort(1)}>تاریخ</td>
              <td onClick={() => handleSort(2)}>نام آگهی</td>
              <td onClick={() => handleSort(3)}>فیلد</td>
              <td onClick={() => handleSort(4)}>مقدار قدیمی</td>
              <td onClick={() => handleSort(5)}>مقدار جدید</td>
            </tr>
          </thead>
          <tbody>
            {itemArray.map(item => (
              <tr key={item.id} className="table-row">
                <td><input 
                      type="checkbox" 
                      className="checkbox" 
                      checked={checked.includes(`${item.id}`)} 
                      onChange={(event: React.ChangeEvent<HTMLInputElement>) => handleChecked(event, `${item.id}`)} /></td>
                <td>{item.name}</td>
                <td>{moment(item.date).format('YYYY/MM/DD')}</td>
                <td>{item.title}</td>
                <td>{item.field}</td>
                <td>{item.oldValue}</td>
                <td>{item.newValue}</td>
                </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
}

export default App;
