import { useState } from 'react';

const useSyncState = (initialValue?: any) => {
  const [value, setValue] = useState(initialValue);
  let latestValue = value ?? null;

  const get = () => latestValue;

  const set = (newValue: unknown) => {
    if (Array.isArray(newValue))
      latestValue = [...newValue]
    else if (typeof newValue === 'object' && newValue !== null)
      latestValue = {...newValue}
    else if (typeof newValue === 'string' || typeof newValue === 'boolean' || typeof newValue === 'symbol' || typeof newValue === 'bigint' || typeof newValue === 'number')
      latestValue = newValue
    setValue(newValue);
  }

  return [get(), set]
}

export default useSyncState;