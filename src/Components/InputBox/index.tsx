import React, { FC, ChangeEvent } from 'react';
import styles from './InputBox.module.scss';
import cn from 'classnames';

interface InputBoxProps {
  title: string,
  placeholder: string,
  input: string,
  onChange: (value: string) => void,
  className?: string
}

const InputBox: FC<InputBoxProps> = ({ title, placeholder, input, onChange, className }) => {

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    onChange(event.currentTarget.value)
  }

  return (
    <div className={cn(styles['container'], className)}>
      <div className={styles['title']}>{title}</div>
      <input className={styles['input']} value={input} placeholder={placeholder} onChange={(event: ChangeEvent<HTMLInputElement>) => handleChange(event)} />
    </div>
  )
}

export default InputBox;